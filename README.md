Moodle Local MentorWeb
======================

Esse plugin integra o MentorWeb com o Moodle para criação de atividades.

Instalação
----------

Na raiz do seu Moodle, execute:

    $ git clone https://gitlab.com/danielneis/moodle-local_mentorweb local/mentorweb
    
Se você não utiliza o git, pode baixar a versão mais atual deste plugin no endereço:

    https://gitlab.com/danielneis/moodle-local_mentorweb/repository/master/archive.zip
    
Ao descompactar este arquivo, será criada uma pasta com arquivos. Certifique-se de colocar esta pasta dentro da sua instalação do Moodle, na pasta "local", com nome "mentorweb".
    
Visite seu Moodle como administrador para finalizar a instalação.

Ao instalar o plugin é necessário fazer duas configurações:

* URL: endereço do web service do MentorWeb
* Token: o token do serviço criado no Moodle para sincronização de dados com o MentorWeb

Utilização
----------

Esse plugin incluindo um novo campo "Tipo de Nota", trazido do MentorWeb, no formulário de cadastro e atualização de todas as atividades.

Ao criar uma atividade, é verificado MentorWeb se a data escolhida é válida e, caso positivo, é criado no MentorWeb uma Avaliação Parcial referente à atividade criada no Moodle.
