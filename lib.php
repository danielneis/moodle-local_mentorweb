<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Validates activities on MentorWeb calendar.
 *
 * @package   local_mentorweb
 * @copyright 2017 Daniel Neis Araujo
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->dirroot.'/local/mentorweb/classes/mentorweb.php');

/**
 * Inject the mentorweb elements into all moodle module settings forms.
 *
 * @param moodleform $formwrapper The moodle quickforms wrapper object.
 * @param MoodleQuickForm $mform The actual form object (required to modify the form).
 */
function local_mentorweb_coursemodule_standard_elements($formwrapper, $mform) {
    global $COURSE;

    $mform->addElement('header', 'mentorwebsection', get_string('mentorweb', 'local_mentorweb'));

    $options = mentorweb::criterios_avaliacao($COURSE->id);
    $mform->addElement('select', 'mentorwebtiponota', get_string('tiponota', 'local_mentorweb'), $options);

    $mform->addElement('text', 'mentorwebsigla', get_string('sigla', 'local_mentorweb'));
    $mform->setType('mentorwebsigla', PARAM_TEXT);
}

/**
 * Hook the validation of the course module.
 *
 * @param stdClass $data Data from the form submission.
 * @param stdClass $course The course.
 */
function local_mentorweb_coursemodule_validation($form, $data) {
    $errors_diario = mentorweb::verifica_diario($data);
    $errors_cadastro = mentorweb::cadastra_avaliacao($data);
    return array_merge($errors_diario, $errors_cadastro);
}

/**
 * Hook before course module delete.
 *
 * @param stdClass $m The course module instance being deleted.
 */
function local_mentorweb_pre_course_module_delete($cm) {
    mentorweb::exclui_avaliacao($cm->id);
}

/**
 * Hook called to check whether async course module deletion should be performed or not.
 *
 * @return false as if background deletion is not required
 */
function local_mentorweb_course_module_background_deletion_recommended() {
    return false;
}
