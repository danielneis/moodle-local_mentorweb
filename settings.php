<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings for MentorWeb integration.
 *
 * @package    local_mentorweb
 * @copyright  2014 Daniel Neis
 * @license    http://www.gnu.org/copyleft/gpl.html gnu gpl v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $settings = new admin_settingpage('local_mentorweb',
                                            get_string('pluginname', 'local_mentorweb'));

    $ADMIN->add('localplugins', $settings);

    $settings->add(new admin_setting_heading('local_mentorweb/settings', '', get_string('pluginname_desc', 'local_mentorweb')));

    $settings->add(new admin_setting_configtext('local_mentorweb/url',
                      get_string('url', 'local_mentorweb'),
                      get_string('url_desc', 'local_mentorweb'),
                      'https://demo.edusoft.com.br/demo/rest/ICorujaACA/'));

    $settings->add(new admin_setting_configtext('local_mentorweb/token',
                            get_string('token', 'local_mentorweb'),
                            get_string('token_desc', 'local_mentorweb'),
                            ''));

}
