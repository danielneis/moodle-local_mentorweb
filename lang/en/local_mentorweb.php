<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for MentorWeb local plugin.
 *
 * @package    local_mentorweb
 * @copyright  2017 Daniel Neis
 * @license    http://www.gnu.org/copyleft/gpl.html gnu gpl v3 or later
 */

$string['pluginname'] = 'MentorWeb';
$string['pluginname_desc'] = 'Integração de atividades com o MentorWeb';
$string['mentorweb'] = 'MentorWeb';
$string['sigla'] = 'Sigla';
$string['tiponota'] = 'Tipo de nota';
$string['token'] = 'Token';
$string['token_desc'] = 'Token para autenticação com o MentorWeb';
$string['url'] = 'URL';
$string['url_desc'] = 'URL do web service do MentorWeb';
