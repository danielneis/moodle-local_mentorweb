<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The class that to deals with MentorWeb.
 *
 * @package    local_mentorweb
 * @copyright  2017 Daniel Neis
 * @license    http://www.gnu.org/copyleft/gpl.html gnu gpl v3 or later
 */

class mentorweb {

    private static function request($path, $fields, $type = "GET") {
        $url = get_config('local_mentorweb', 'url');
        $token = get_config('local_mentorweb', 'token');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url.$path,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $fields,

            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "token: {$token}"
            )
        ));
        $raw = curl_exec($curl);
        $response = json_decode($raw);
        $error = curl_error($curl);
        return array($response, $error);
    }

    public static function cadastra_avaliacao($data) {

        $fields = new stdclass();
        $fields->codigoIntegracaoTdi = $data['course'];
        $fields->idTipoNota = $data['mentorwebtiponota'];
        $fields->sigla = $data['mentorwebsigla'];
        $fields->descricao = $data['name'];
        $fields->conteudo = $data['name'];
        $fields->obrigatorioParaCalculo = 1;
        $fields->codigoIntegracaoAvaliacao = $data['coursemodule'];
        $fields->data = date('d/m/Y', $data['duedate']);
        $fields = json_encode($fields, JSON_UNESCAPED_SLASHES);

        list($response, $error) = self::request('cadastraAvaliacaoParcial', $fields, 'POST');
        $errors = array();
        if (isset($response->codigoRetornoIntegracao) &&
            $response->codigoRetornoIntegracao != 'ICA_00000') {
            if (isset($response->descricaoRetornoIntegracao)) {
               $errors['mentorwebtiponota'] = $response->descricaoRetornoIntegracao;
            }
            if (isset($response->observacaoRetornoIntegracao)) {
               $errors['mentorwebtiponota'] .= " : " . $response->observacaoRetornoIntegracao;
            }
        }
        return $errors;
    }

    public static function exclui_avaliacao($cmid) {

        $fields = new stdclass();
        $fields->codigoIntegracaoAvaliacao = $cmid;
        $fields = json_encode($fields);

        return self::request('excluiAvaliacaoParcial', $fields, 'POST');
    }

    public static function verifica_diario($data) {

        $fields = new stdclass();
        $fields->codigoIntegracao = $data['course'];
        $fields->idTipoNota = $data['mentorwebtiponota'];
        $fields->dataInicial = date('d/m/Y', $data['allowsubmissionsfromdate']);
        $fields->dataFinal = date('d/m/Y', $data['duedate']);
        $fields = json_encode($fields);

        list($response, $error) = self::request('realizaVerificacaoDiarioDeClasse', $fields, 'POST');

        $errors = array();
        if (isset($response->codigoRetornoIntegracao) &&
            $response->codigoRetornoIntegracao != 'ICA_00000') {
           if (isset($response->observacaoRetornoIntegracao)) {
               $errors['mentorwebtiponota'] = $response->observacaoRetornoIntegracao;
           } else {
               $errors['mentorwebtiponota'] = 'Erro desconhecido.';
           }
        }
        return $errors;
    }

    public static function criterios_avaliacao($courseid) {

        $fields = new stdclass();
        $fields->tipoNotaRetornar = 1;
        $fields->codigoIntegracao = $courseid;
        $fields = json_encode($fields);

        list($response, $error) = self::request('consultaCriterioAvaliacaoPorTurmaDisciplina', $fields, 'POST');

        if ($error) {
            return array();
        } else {
            $result = array('' => 'Selecione...');
            if (isset($response->listaTipoNota)) {
              foreach ($response->listaTipoNota as $tipo_nota) {
                  $result[$tipo_nota->idTipoNota] = $tipo_nota->sigla . " - " . $tipo_nota->descricaoTipoNota;
              }
            }
            return $result;
        }
    }

    public static function atualiza_notas($data) {

        $logs = array();
        foreach ($data as $d) {
            $fields = json_encode($d);
            list($response, $error) = self::request('gravaNotaAvaliacaoParcialParaListaDeAlunos', $fields, 'POST');
            $logs[] = array('request' => $fields, 'response' => $response, 'error' => $error);
        }
        return $logs;
    }
}
